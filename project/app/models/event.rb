class Event < ActiveRecord::Base
  mount_uploader :event_image, EventImageUploader

  has_many :tickets, dependent: :destroy#親のオブジェクトのレコードが削除されたら同時に削除
  belongs_to :owner, class_name: 'User'

  has_many :event_attachments, dependent: :destroy
  accepts_nested_attributes_for :event_attachments

  validates :name, length: { maximum: 50 }, presence: true
  validates :place, length: { maximum: 100 }, presence: true
  validates :content, length: { maximum: 2000 }, presence: true
  validates :start_time, presence: true
  validates :end_time, presence: true
  validate  :start_time_should_be_before_end_time

  before_save :password_to_digest

  def created_by?(user)
    return false unless user
    owner_id == user.id
  end

  def authenticate(password)
    return false if self.password.blank? || password.blank?
    Digest::MD5.new.update(password).to_s == self.password
  end

  private

  def password_to_digest
    if self.password.blank?
      self.password = nil
    else
      self.password = Digest::MD5.new.update(self.password).to_s
    end
    self
  end

  def start_time_should_be_before_end_time
    return unless start_time && end_time

    if start_time >= end_time
      errors.add(:start_time, 'は終了時間よりも前に設定してください')
    end
  end
end
