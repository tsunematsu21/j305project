require 'file_size_validator'

class EventAttachment < ActiveRecord::Base
  mount_uploader :url, EventAttachmentUploader
  belongs_to :event

  validates :url, presence: true, on: :create, file_size: { maximum: 5.megabytes.to_i }
end
