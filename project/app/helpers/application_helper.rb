module ApplicationHelper
  @@renderer = Redcarpet::Render::HTML.new(link_attributes: {target: '_blank'}, hard_wrap: true)
  # ユーザのTwitterのURLを返す
  def url_for_twitter(user)
    "https://twitter.com/#{user.nickname}"
  end

  def markdown(text)
    unless @markdown
      
      @markdown = Redcarpet::Markdown.new(@@renderer, autolink: true)
    end

    @markdown.render(text).html_safe
  end
end
