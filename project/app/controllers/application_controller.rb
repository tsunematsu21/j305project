class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :current_user, :logged_in?

  # BASIC認証
  # http_basic_authenticate_with :name => "tec", :password => "george"

  # エラーハンドリング
  rescue_from ActiveRecord::RecordNotFound, ActionController::RoutingError, with: :error404
  rescue_from Exception, with: :error500

  # 1ページに表示する件数
  PER = 10

  before_action :check_user_agent

  private

  # ログインしているユーザを取得
  def current_user
    return unless session[:user_id] #ログインしていなければ
    @current_user ||= User.find(session[:user_id])
  end

  # ログインしているかどうか
  def logged_in?
  	!!session[:user_id]
  end

  # ログインしていなければトップページにリダイレクト
  def authenticate
    return if logged_in?
    redirect_to root_path, alert: 'ログインしてください'
  end

  # 検索フォームの値をチェック
  def search_params
    p = params.require(:q).permit(
      :name_or_place_cont_any, :start_time_gteq, :owner_id_not_eq, :s, :name_cont_all, :place_cont_all
    )

    p[:name_cont_all]  = p[:name_cont_all].split(/ |　/)
    p[:place_cont_all] = p[:place_cont_all].split(/ |　/)
    p[:s] ||= 'start_time'
    return p
  rescue
    s = params.require(:q).permit(:s) rescue {s: 'start_time'}
    s[:start_time_gteq] = Date.today
    return s
  end

  def check_user_agent
    ua = request.env["HTTP_USER_AGENT"]
    if ua.include?('MSIE')
      flash[:alert] = "Internet Explorerでは正常に表示されない場合がございます。ご了承ください。"
    end
  end

  # ユーザーの現在のURLを保存
  def store_location
    session[:return_to] = request.fullpath
  end

  def error404(e)
    render 'error404', status: 404, formats: [:html]
  end

  def error500(e)
    logger.error [e, *e.backtrace].join("\n")
    render 'error500', status: 500, formats: [:html]
  end
end