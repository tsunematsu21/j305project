class EventsController < ApplicationController
  before_action :authenticate, except: [:show, :index, :password] #show以外はログインさせる
  before_action :store_location, except: [:password, :destroy]

  # 1ページに表示する件数
  PER = 10

  # 添付できるファイルの最大数
  EVENT_ATTACHMENT_LIMIT = 5

  def index
    @q = Event.where("password is ?", nil).order(:start_time).page(params[:page]).per(PER).search(search_params)
    @events = @q.result(distinct: true)
  end

  def show
    @event = Event.find(params[:id])

    unless @event.owner == current_user || @event.password.nil?
      pass = params[:password]
      path = page_authenticate_path({ url: event_path(@event) })

      return redirect_to path if pass.nil?
      return redirect_to path, alert: 'パスワードが違います！' unless @event.authenticate(pass)
    end

    @ticket  = current_user && current_user.tickets.find_by(event_id: params[:id])
    @tickets = @event.tickets.includes(:user).order(:created_at) # イベントに参加したユーザ一覧を作成時間順に表示
  end

  def new
    @event = current_user.created_events.build
    @event_attachments = @event.event_attachments.build
  end

  def create
    @event = current_user.created_events.build(event_params)
    
    if @event.save
      error_msg = create_event_attachments( params[:event_attachments_new] )

      unless error_msg.blank?
        flash.now[:alert] = error_msg
        render :new and return
      end

      redirect_to @event, notice: '作成しました'
    else
      render :new
    end
  end

  def edit
    @event = current_user.created_events.find(params[:id])
    @event_attachments = @event.event_attachments
  end

  def update
    @event = current_user.created_events.find(params[:id])

    if @event.update(event_params)
      error_msg = create_event_attachments( params[:event_attachments_new] )

      unless error_msg.blank?
        flash.now[:alert] = error_msg
        render :edit and return
      end

      unless params[:event_attachments].blank?
        params[:event_attachments]['remove_id'].each do |id|
          @event.event_attachments.find(id).destroy! unless id == '0'
        end
      end
      redirect_to @event, notice: '更新しました'
    else
      render :edit
    end
  end

  def destroy
    @event = current_user.created_events.find(params[:id])
    @event_name = @event.name
    @event.destroy!
    redirect_to root_path, notice: "「#{@event_name}」を削除しました"
  end

  def password
    @url = params[:url]
  end

  private

  def event_params
    params.require(:event).permit(
      :name, :place, :event_image, :event_image_cache, :remove_event_image, :content, :start_time, :end_time, :password,
      :event_attachments_new
    )
  end

  def create_event_attachments(event_attachments_new)
    return "" if event_attachments_new.blank?
    limit     = EVENT_ATTACHMENT_LIMIT - @event.event_attachments.count
    error_msg = ""

    event_attachments_new['url'].each do |url|
      break if --limit < 0
      @event_attachment = @event.event_attachments.build(:url => url, :event_id => @event.id)
      unless @event_attachment.save
        @event_attachment.errors.full_messages.each { |msg| error_msg += msg }
        @event_attachment.delete
      end
    end

    return error_msg
  end
end
