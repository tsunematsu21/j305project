class SessionsController < ApplicationController
	def create
		user = User.find_or_create_from_auth_hash(request.env['omniauth.auth'])
		session[:user_id] = user.id

		session[:return_to] = mypage_path if session[:return_to] == root_path
    redirect_to session[:return_to] || root_path, notice: "@#{user.nickname}でログインしました"
    session[:return_to] = nil
	end

	def destroy
		reset_session
		redirect_to root_path, notice: 'ログアウトしました'
	end
end