class WelcomeController < ApplicationController
  before_action :store_location

  def index
    @events = Event.where('password is ?', nil).search({ start_time_gteq: Time.zone.now }).result
    @new_events = @events.order("created_at DESC").first(5)
    @popular_events = @events.joins(:tickets).select('*, events.id as id, count(*) as CNT').group(:id).order('CNT DESC').first(5)
  end

  def check_login_and_redirect
    if logged_in?
      redirect_to params.require(:path)
    else
      session[:return_to] = params.require(:path)
      redirect_to root_path + 'auth/twitter'
    end
  rescue
    redirect_to root_path
  end
end