class UsersController < ApplicationController
  before_action :authenticate

  def retire
  end

  def destroy
    if current_user.destroy
      reset_session
      redirect_to root_path, notice: '退会完了しました'
    else
      render retire
    end
  end

  def mypage
    @tickets = current_user.tickets.includes(:event)

    event_id_array = Array.new
    @tickets.each do |t|
      event_id_array << t.event_id
    end

    events_arel = Event.arel_table
    events_sel  = events_arel[:owner_id].eq(current_user.id)

    event_id_array.each do |event_id|
      events_sel = events_sel.or(events_arel[:id].eq(event_id))
    end

    hold_and_join_events = Event.where(events_sel).order(:start_time)

    @start_year = hold_and_join_events.first.start_time.year unless hold_and_join_events.first.nil?
    @end_year   = hold_and_join_events.last.start_time.year  unless hold_and_join_events.last.nil?

    @q = hold_and_join_events.page(params[:page]).per(PER).search(search_params)
    @events = @q.result(distinct: true)
  end
end