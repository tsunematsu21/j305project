# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

onReady = ->
  $('.year, .month').change (event) ->
    context = $(event.target).parent()
    y = $('.year', context).val()
    m = $('.month', context).val() - 1
    for d in [ 29, 30, 31 ]
      $(".day > option[value='#{ d }']", context)[ if new Date(y, m, d).getMonth() == m then 'show' else 'hide' ]()
    if $('.day > option:selected', context).css('display') == 'none'
      $('.day', context).val $('.day > option', context).filter((i, e) -> $(e).css('display') != 'none').last().val()

  $('.year').change()

  $('.js-img-upload').fileinput
	  showPreview: false
	  maxFileCount: 1
	  browseClass: 'btn btn-info fileinput-browse-button'
	  browseIcon: ''
	  browseLabel: ' ファイル選択'
	  removeClass: 'btn btn-warning'
	  removeIcon: ''
	  removeLabel: ' キャンセル'
	  showUpload: false
	  layoutTemplates: { icon: '' }

$(onReady)
$(document).on('page:load', onReady)