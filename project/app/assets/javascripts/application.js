// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require bootstrap.min
//= require bootstrap-fileinput
//= require_tree .

//リンク先までスクロール


function ready(){
  $('a[href^=#]').on('click', function(event){
    event.preventDefault(); //イベントをキャンセル
    var target = $(this).attr('href');
    $('html, body').animate({scrollTop: $(target).offset().top}, 500);
  });

  $(".fileinput").change(fileInputChange);
  $(".filecheckbox").click(fileCheckboxClick);

  if( $(".fileinput").length + $(".filecheckbox").length > 5 ){
    $(".fileinput").last().hide();
  }
}

$(document).ready(ready);
$(document).on('page:load', ready);




function fileInputChange(){
  if($(".fileinput").last().val() != "" && checkUploadLimit() < 5 ){
    $("#filelist").append('<input type="file" name="event_attachments_new[url][]" class="fileinput js-img-upload">')
    .bind('change', fileInputChange);
  }
}

function checkUploadLimit(){
  var checked = 0;
  $('[class="filecheckbox"]:checked').map(function(){
    if( $(this).val() )checked++;
  });

  var inputs = 0;
  $(".fileinput:visible").map(function(){
    if( $(this).val() )inputs++;
  });

  console.log( inputs + ($(".filecheckbox").length - checked) );
  return $(".fileinput:visible").length + ($(".filecheckbox").length - checked);
}

function fileCheckboxClick(){
  var $fileinput = $(".fileinput").last();

  if( checkUploadLimit() <= 5 ){
    $fileinput.show();
      if($(".fileinput").last().val() != "" && checkUploadLimit() < 5 ){
      $("#filelist").append('<input type="file" name="event_attachments_new[url][]" class="fileinput js-img-upload">')
        .bind('change', fileInputChange);
    }
  }else{ 
    $fileinput.hide();
    $fileinput.replaceWith( $fileinput.clone() );
  }
}