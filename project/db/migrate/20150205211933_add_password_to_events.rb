class AddPasswordToEvents < ActiveRecord::Migration
  def change
    add_column :events, :password, :string, null: true, default: nil
  end
end
