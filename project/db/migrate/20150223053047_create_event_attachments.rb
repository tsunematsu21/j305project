class CreateEventAttachments < ActiveRecord::Migration
  def change
    create_table :event_attachments do |t|
      t.integer :event_id, null: false
      t.string  :url,      null: false

      t.timestamps
    end

    add_index :event_attachments, :event_id
  end
end
