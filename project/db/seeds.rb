# coding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

#names = ['google', 'yahoo', 'github', 'bitbucket', 'rails', 'ruby']
#names.each do |name|
#  User.create(:provider => 'test', :uid => name, :nickname => name, :image_url=> "#{name}.png")
#end

#event_names = ['あいうえお', 'かきくけこ', 'さしすせそ', 'たちつてと', 'なにぬねの']
#for i in 0..event_names.length
#  Event.create(:owner_id => 8,
#               :name => "piyopiyo Event #{i+1}",
#               :place => "どこか",
#               :start_time => "2016-01-0#{i+1} 00:00:00",
#               :end_time => "2016-01-0#{i+1} 23:59:00",
#               :content => event_names[i],
#               :event_image => nil)
#end
#Event.create(:owner_id => 1, :name => '古い', :place => "どこか", :start_time => "2013-01-01 00:00:00", :end_time => "2013-01-03 00:00:00", :content => 'a', :event_image => nil, :password => '')
